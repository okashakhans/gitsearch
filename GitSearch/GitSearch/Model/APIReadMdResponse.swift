//
//  APIReadMdResponse.swift
//  GitSearch
//
//  Created by Okasha Khan on 20/05/2021.
//

import Foundation

struct APIReadMdResponse: Codable {
    
    let name: String?
    let download_url: String?
}
