//
//  RealmRespository.swift
//  GitSearch
//
//  Created by Okasha Khan on 20/05/2021.
//

import RealmSwift


@objcMembers class RealmRepository: Object {
    
    dynamic var id: Int? = nil
    dynamic var name: String? = nil
    
    dynamic var forks: Int? = nil
    dynamic var open_issues: Int? = nil
    dynamic var watchers_count: Int? = nil
    
    dynamic var url: String? = nil
    dynamic var owner: RealmOwner? = nil
    dynamic var date: Date? = nil
    
    
    convenience init(_ repository: Repository) {
        
        self.init()
        
        id =   repository.id
        name =   repository.name
        forks =   repository.forks
        open_issues =   repository.open_issues
        watchers_count =   repository.watchers_count
        url =   repository.url
        owner = RealmOwner(repository.owner)
        date = Date()
        
    }
    
    override static func primaryKey() -> String? {
           return "name"
    }
}


@objcMembers class RealmOwner: Object {
    
    dynamic var login: String? = nil
    dynamic var avatar_url: String? = nil
    
    
    convenience init(_ owner: Owner) {
        self.init()
        
        login =   owner.login
        avatar_url =   owner.avatar_url
    }
    
}


