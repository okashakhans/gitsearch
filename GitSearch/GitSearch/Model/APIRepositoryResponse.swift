//
//  APIRepositoryResponse.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import Foundation

struct APIRepositoryResponse: Codable {
    
    let items: [Repository]
    
}
