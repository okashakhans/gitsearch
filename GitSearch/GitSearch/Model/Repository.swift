//
//  Repository.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import Foundation

struct Repository: Codable {
    
    let id: Int
    let name:String
    let forks:Int
    let open_issues: Int
    let watchers_count:Int
    let url:String
    let owner:Owner
}

struct Owner: Codable {
   
    let login:String
    let avatar_url:String
}
