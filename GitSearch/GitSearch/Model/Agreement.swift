//
//  Agreement.swift
//  GitSearch
//
//  Created by Okasha Khan on 21/05/2021.
//

import Foundation



struct Agremement {
    
    static let text = "Git Search is for educational purposes. \n\n\nIt is integrated with GitHub free API that enables users to search repositories. \n\n\nGit Search values your privacy. All files are saved locally and are not processed in any manners. All your personal information is not stored at any time of use.\n\n\nPlease confirm you understand and accept the git search privacy statement and service agreement by accepting and if you don’t agree then please remove this app"
}
