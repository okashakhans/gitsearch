//
//  DisplayData.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import UIKit



/// Used in Search Detail view controller
struct DisplayTitle {
    
    let title: String
    let results: DisplayType
}

/// Used in Search view controller and Search result view controller
enum DisplayType {
    
    case details(model:Repository)
    case readmd(model:ReadMd)
}

/// Used in search detail view controllers for detail section.
struct DetailUI {
   
    let title:String
    let url:String
    let color:UIColor
    
}
