//
//  AppDelegate.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        self.checkUsageAgreement()
        
        return true
    }
    
    func checkUsageAgreement(){
        
        let userDefaults = UserDefaults.standard
        if userDefaults.bool(forKey: "accepted") == false {
            
            self.displayAgreementViewController()
            
        }else{

            self.displayTabbarViewController()
        }
    }

    private func displayAgreementViewController(){
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        let navVC = UINavigationController(rootViewController: AppViewController())
        navVC.navigationBar.prefersLargeTitles = true
        navVC.viewControllers.first?.navigationItem.largeTitleDisplayMode = .always
        window.rootViewController = navVC
        window.makeKeyAndVisible()
        self.window = window
    }
    
    private func displayTabbarViewController(){
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = TabbarViewController()
        window.makeKeyAndVisible()
        self.window = window
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

