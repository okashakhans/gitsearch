//
//  RepositoryCollectionViewCell.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "RepositoryCollectionViewCell"

    // MARK: - Setups
    private let repositoryImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "photo")
        imageView.contentMode = .scaleAspectFill
    
        return imageView
    }()

    private let repositoryNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 18, weight: .regular)
        return label
    }()

    private let repositoryUserName: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 15, weight: .thin)
        return label
    }()

    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .secondarySystemBackground
        contentView.backgroundColor = .secondarySystemBackground
        contentView.addSubview(repositoryImage)
        contentView.addSubview(repositoryNameLabel)
        contentView.addSubview(repositoryUserName)
        contentView.clipsToBounds = true
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.imageSetup()
        self.labelSetup()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        repositoryNameLabel.text = nil
        repositoryImage.image = nil
        repositoryUserName.text = nil
    }

    
    // MARK: - Funcitons
    
    private func imageSetup(){
        
        let imageSize: CGFloat = contentView.height-10
        repositoryImage.frame = CGRect(x: 5,y: 2, width: imageSize, height: imageSize)
        repositoryImage.layer.cornerRadius = imageSize/2
        repositoryImage.layer.masksToBounds = true
    }
    
    private func labelSetup(){
        
        repositoryNameLabel.frame = CGRect(x: repositoryImage.right+10,y: 0,width: contentView.width-repositoryImage.right-15,height: contentView.height/2)
        repositoryUserName.frame = CGRect(
            x: repositoryImage.right+10,
            y: contentView.height/2,
            width: contentView.width-repositoryImage.right-15,
            height: contentView.height/2
        )
    }
    
    
    // MARK: - External Callers
    
    func configure(with repositoryModel: Repository) {

        repositoryNameLabel.text = repositoryModel.name
        repositoryUserName.text = repositoryModel.owner.login
        repositoryImage.sd_setImage(with: URL(string:repositoryModel.owner.avatar_url), completed: nil)
   }
    
}

