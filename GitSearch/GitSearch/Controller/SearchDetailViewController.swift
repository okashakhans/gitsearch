//
//  SearchDetailViewController.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import UIKit

class SearchDetailViewController: UIViewController {
    

    // MARK: - Setups
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)

        tableView.register(UITableViewCell.self,forCellReuseIdentifier: "cell")
        tableView.register(SearchDetailTableViewCell.self,forCellReuseIdentifier: SearchDetailTableViewCell.identfier)

        return tableView
    }()
    
    private var displayData:[DisplayTitle] = []
    private var detailDisplayData = [DetailUI]()// This is to represent different type of images and titles in detail section.
    private let repository: Repository
    
    // MARK: - Lifecycle
    
    init(repository: Repository) {
        
        self.repository = repository
        self.displayData.append(DisplayTitle(title: "Detail", results:DisplayType.details(model: repository)))
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground

        view.addSubview(tableView)
        
        self.title = repository.name
        
        self.createTableHeader(with: repository.owner.avatar_url, name: repository.owner.login)
        self.updateUI()
        self.fetchReadMd(with: repository.url)
        RealmManager.shared.saveRepository(repository: repository)
        RealmManager.shared.preserverRealmReposotries(with: 9)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    
    
    // MARK: - Functions
    
    /// The function will fetc read md file if exist
    /// - Parameter urlString: content folder url
    private func fetchReadMd(with urlString:String?){
        
        guard let urlString = urlString else { return }
        
        APIManager.shared.readMdExistFetchItwith(with: urlString) { result in
            
            switch result {
            
                case .success(let results):
                    
                     self.displayData.append(DisplayTitle(title: "README.md", results:DisplayType.readmd(model: results)))
                     self.tableView.reloadData()
                    
                case .failure(let error):  print("Error \(error)")
            }
        }
        
    }
    
    /// The function will configure display model
    /// - Note: The function will unhide table view and reload data.
    private func updateUI() {
        
        detailDisplayData.append(DetailUI(title: "Issues \(repository.open_issues)", url: "exclamationmark.square.fill", color: UIColor.systemGreen))
        detailDisplayData.append(DetailUI(title: "Forks \(repository.forks)", url: "shuffle.circle.fill", color: UIColor.systemBlue))
        detailDisplayData.append(DetailUI(title: "Watchers \(repository.watchers_count)", url: "eye.circle.fill", color: UIColor.systemYellow))
        
        tableView.isHidden = false
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    
    // MARK: - Table views Configuration
    
    private func createTableHeader(with string: String?, name:String?) {
        
        guard let urlString = string, let url = URL(string: urlString) else {
            return
        }

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: view.width/1.5))

        let imageSize: CGFloat = headerView.height/2
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: imageSize, height: imageSize))
        imageView.center = headerView.center
        imageView.contentMode = .scaleAspectFill
        imageView.sd_setImage(with: url, completed: nil)
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageSize/2
        imageView.backgroundColor = UIColor.green
        
        headerView.addSubview(imageView)
        
        let label = UILabel()
        label.numberOfLines = 1
        label.font = .systemFont(ofSize: 22, weight: .semibold)
        label.textAlignment = .center
        let ycalculate = CGFloat(imageView.height + (imageView.height/2) + 20)
        label.frame = CGRect(x:0, y:ycalculate, width:headerView.width, height:28)
        label.text = name
        
        headerView.addSubview(label)
    
        tableView.tableHeaderView = headerView
    }
}

    // MARK: - Extensions (Table View)

extension SearchDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let result =  displayData[section].results
        
        switch result {
            case .details: return 3
            case .readmd: return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let result =  displayData[indexPath.section].results
        
        switch result {
        
            case .details:
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchDetailTableViewCell.identfier,for: indexPath) as? SearchDetailTableViewCell else {return  UITableViewCell()}
                cell.configure(with: detailDisplayData[indexPath.row])
                return cell
                
            case .readmd(let model):

                let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as UITableViewCell
                    
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.text = model.file
                    
                return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let model = displayData[section]
        return model.title
    }
}
    
