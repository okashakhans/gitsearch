//
//  TabbarViewController.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import UIKit

class TabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let vc = SearchViewController()

        vc.title = "Search"
        vc.navigationItem.largeTitleDisplayMode = .always

        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.tintColor = .label
        nav.tabBarItem = UITabBarItem(title: "Search", image: UIImage(systemName: "magnifyingglass"), tag: 1)
        nav.navigationBar.prefersLargeTitles = true

        setViewControllers([nav], animated: false)
        
    }


}
