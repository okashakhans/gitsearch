//
//  SearchResultViewController.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import Foundation
import UIKit
import SDWebImage

protocol SearchResultsViewControllerDelegate: AnyObject {
    
     func didTap(with selectedRepository: Repository)
}


class SearchResultsViewController: UIViewController {
    
    // MARK: - Setups
    
    weak var delegate: SearchResultsViewControllerDelegate?
    
    private var collectionView: UICollectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewCompositionalLayout { sectionIndex, _ -> NSCollectionLayoutSection? in
            return SearchResultsViewController.createSectionLayout(section: sectionIndex)
        }
    )
    
    
    private var displayData: [DisplayType] = []
    
    
    // MARK: - Lifecycle
        
    override func viewDidLoad() {
    super.viewDidLoad()

        view.backgroundColor = .systemBackground

    }

    override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    // MARK: - Functions
    
    /// The function will show the data in collection view.
    /// - Note: it will convert data according to the requirement of displaying.
    /// - Note: it will called from search view controller.
    /// - Parameter data: repositories lists
    public func updateUI(with data:[Repository]){
        
        //self.displayData = [DisplayType(data.compactMap({.deta}))]
        self.displayData.removeAll()
        self.displayData.append(contentsOf: data.compactMap({.details(model: $0)}))
       
        self.configureCollectionView()
        collectionView.reloadData()
    }
    
    
    
    // MARK: - Collection views Configuration
    
    private func configureCollectionView() {
        view.addSubview(collectionView)
        
        collectionView.register(UICollectionViewCell.self,forCellWithReuseIdentifier: "cell")
        collectionView.register(SearchCollectionViewCell.self,forCellWithReuseIdentifier: SearchCollectionViewCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .systemBackground
    }
    
    static func createSectionLayout(section: Int) -> NSCollectionLayoutSection {
        
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
        )

        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)

        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .absolute(80)
            ),
            subitem: item,
            count: 1
        )

        let section = NSCollectionLayoutSection(group: group)
        return section
    }
}

// MARK: - Extensions (Collection View)

extension SearchResultsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return displayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let result = displayData[indexPath.row]
        
        switch result {
        
            case.details(model: let repository):
                
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchCollectionViewCell.identifier,for: indexPath) as? SearchCollectionViewCell else {return UICollectionViewCell()}

                cell.configure(with: repository)

                return cell
                
            case.readmd: break
        }
       return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let result = displayData[indexPath.row]
        
        switch result {
        
            case.details(model: let repository):
                
            self.delegate?.didTap(with: repository)
                
            case.readmd: break
        }
    }
}
