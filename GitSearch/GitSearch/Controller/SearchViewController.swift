//
//  SearchViewController.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import UIKit
import RealmSwift

class SearchViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    // MARK: - Setups
    
    let searchController: UISearchController = {
        let vc = UISearchController(searchResultsController: SearchResultsViewController())
        vc.searchBar.placeholder = " Search Repositories"
        vc.searchBar.searchBarStyle = .minimal
        vc.definesPresentationContext = true
        return vc
    }()
    
    private var collectionView: UICollectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewCompositionalLayout { sectionIndex, _ -> NSCollectionLayoutSection? in
            return SearchResultsViewController.createSectionLayout(section: sectionIndex)
        }
    )
    
    private var displayData: [DisplayType] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
    
        self.configureSearchBar()
        self.fetchRealmRepositories()
        
        //RealmManager.shared.deleteAllRealRepositories()
        //RealmManager.shared.countRealmReposiotires()
        //RealmManager.shared.preserverRealmReposotries(with: 10)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }

   
    // MARK: - Functions
    
    /// the function will fetch realm repositories and display on UI
    private func fetchRealmRepositories(){
        
        RealmManager.shared.getAllRealmRepositories() { result in
            
            switch result {
            
                case .success(let results):
                    self.displayData.removeAll()
                    self.displayData.append(contentsOf: results.compactMap({.details(model: $0)}))
                    self.configureCollectionView()
                    self.collectionView.reloadData()
                     
                case .failure: break
            }
        }
    }
    
    
    /// The function will call API Manager to search for repositories
    /// - Note The function is called when search button is clicked
    /// - Note The function will update result on SearchResultsViewController
    /// - Parameters:
    ///   - query: query is the keyword to search for.
    ///   - resultviewcontroller: search result controller delegate reference to update UI.
    private func searchRepositories(with query:String, resultviewcontroller: SearchResultsViewController){
        
        APIManager.shared.searchRepositories(with: query) { result in
            
            switch result {
                case .success(let results):
                    
                 resultviewcontroller.updateUI(with: results)
                      
                case .failure(let error):  print("Error \(error)")
            }
        }
    }
    
    
    // MARK: - Collection views Configuration

    private func configureCollectionView() {
        view.addSubview(collectionView)
        
        collectionView.register(UICollectionViewCell.self,forCellWithReuseIdentifier: "cell")
        collectionView.register(SearchCollectionViewCell.self,forCellWithReuseIdentifier: SearchCollectionViewCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .systemBackground
    }

    static func createSectionLayout(section: Int) -> NSCollectionLayoutSection {

        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
        )

        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)

        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .absolute(80)
            ),
            subitem: item,
            count: 1
        )

        let section = NSCollectionLayoutSection(group: group)
        return section
    }
    
   
    
    // MARK: - Search Bar Functions
    
    private func configureSearchBar(){
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        guard let resultsController = searchController.searchResultsController as? SearchResultsViewController,
        let query = searchBar.text,!query.trimmingCharacters(in: .whitespaces).isEmpty else { return }

        resultsController.delegate = self

        self.searchRepositories(with: query, resultviewcontroller: resultsController)
    }

    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.fetchRealmRepositories()
    }


}

// MARK: -  Extensions

extension SearchViewController: SearchResultsViewControllerDelegate {
    
    func didTap(with selectedRepository: Repository) {
        
        let vc = SearchDetailViewController(repository: selectedRepository)
        vc.title = selectedRepository.name
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - Extensions (Collection View)

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return displayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let result = displayData[indexPath.row]
        
        switch result {
        
            case.details(model: let repository):
                
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchCollectionViewCell.identifier,for: indexPath) as? SearchCollectionViewCell else {return UICollectionViewCell()}

                cell.configure(with: repository)

                return cell
                
            case.readmd: break
        }
       return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let result = displayData[indexPath.row]
        
        switch result {
        
            case.details(model: let repository):
                
                let vc = SearchDetailViewController(repository: repository)
                vc.title = repository.name
                vc.navigationItem.largeTitleDisplayMode = .never
                navigationController?.pushViewController(vc, animated: true)
                
            case.readmd: break
        }
    }
}

