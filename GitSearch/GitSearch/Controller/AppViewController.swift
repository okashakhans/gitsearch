//
//  AppViewController.swift
//  GitSearch
//
//  Created by Okasha Khan on 21/05/2021.
//

import UIKit

class AppViewController: UIViewController {

    
    let userDefaults = UserDefaults.standard
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.checkDetails()
    }
    
    
    // MARK: - Funcitons
    
    private func checkDetails(){
        
        if userDefaults.bool(forKey: "accepted") == false {
        
            self.displayAgreementViewController()
            
        }else{

            self.displayTabbarViewController()
        }
    }
    
    private func displayAgreementViewController(){
        
        let vc = UINavigationController(rootViewController: AgreementViewController())       
        vc.title = "Term of use"
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.present(vc, animated: true)
    }
    
    private func displayTabbarViewController(){
        
        let mainAppTabBarVC = TabbarViewController()
        mainAppTabBarVC.modalPresentationStyle = .fullScreen
        present(mainAppTabBarVC, animated: true)
    }
}
