//
//  AgreementViewController.swift
//  GitSearch
//
//  Created by Okasha Khan on 21/05/2021.
//

import UIKit

class AgreementViewController: UIViewController {

    
    
    // MARK: - Setups

    private let textView: UITextView = {
        let textView = UITextView()
        textView.text = Agremement.text
        textView.font = UIFont.systemFont(ofSize: 17.0, weight: .medium)
        return textView
    }()
    
    private let acceptButton: UIButton = {
        let acceptButton = UIButton()
         acceptButton.backgroundColor = UIColor.label
        acceptButton.setTitle("Accept", for: .normal)
        acceptButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18.0)
        acceptButton.addTarget(self,action: #selector(accepted),for: .touchUpInside)
        
        return acceptButton
    }()
    
    
    
    
    // MARK: - Lifecycle
    
    init() {
    
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
       
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Terms of use"
        
        view.backgroundColor = .systemBackground
    
        view.addSubview(acceptButton)
        view.addSubview(textView)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureTextView()
        configureButton()
       
    }
    

    // MARK: - Funcitons
    
    private func configureButton(){
        
        acceptButton.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            
            acceptButton.heightAnchor.constraint(equalToConstant: 50),
            acceptButton.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 30),
            acceptButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            acceptButton.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -30)
        ]

        NSLayoutConstraint.activate(constraints)
        
        acceptButton.layer.cornerRadius = 12
        
    }
    
    private func configureTextView(){
        
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            
            textView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            textView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 8),
            textView.bottomAnchor.constraint(equalTo: acceptButton.safeAreaLayoutGuide.topAnchor, constant: -20 ),
            textView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -8)
        ]

        NSLayoutConstraint.activate(constraints)
    }
    
    private func configureNavigationController(){
        
        navigationItem.title = "Terms of use"

        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)

        self.navigationController?.navigationBar.prefersLargeTitles = false

        self.navigationController?.navigationItem.largeTitleDisplayMode = .never
    }
    
    @objc private func accepted(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "accepted")
        userDefaults.synchronize()
        self.displayTabbar()
        
    }
    
    
    private func displayTabbar(){
        
        let mainAppTabBarVC = TabbarViewController()
        mainAppTabBarVC.modalPresentationStyle = .fullScreen
        present(mainAppTabBarVC, animated: true)
    }
}
