//
//  SearchDetailTableViewCell.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//


import UIKit

class SearchDetailTableViewCell: UITableViewCell {
    
    
    // MARK: - Setups

    static let identfier = "SearchDetailTableViewCell"
    
    private let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        return label
    }()

    private let iconImageViewe: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(label)
        contentView.addSubview(iconImageViewe)
        contentView.clipsToBounds = true
        accessoryType = .disclosureIndicator
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.imageSetup()
        self.labelSetup()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
    // MARK: - Funcitons
    
    private func imageSetup(){
        
        let imageSize: CGFloat = contentView.height-10
        iconImageViewe.frame = CGRect(
            x: 2,
            y: 5,
            width: imageSize,
            height: imageSize
        )
        iconImageViewe.layer.cornerRadius = 8
        iconImageViewe.layer.masksToBounds = true
    }
    
    private func labelSetup(){
        
        label.frame = CGRect(x: iconImageViewe.right+10, y: 0, width: contentView.width-iconImageViewe.right-15, height: contentView.height)
    }
    
    
    // MARK: - External Callers
    
    func configure(with model:DetailUI) {
        
        self.iconImageViewe.image = UIImage(systemName: model.url)
        self.iconImageViewe.tintColor = model.color
        self.label.text = model.title
        self.selectionStyle = .none
        accessoryType = .none
    }
    
}
