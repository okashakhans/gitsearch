//
//  RealmManager.swift
//  GitSearch
//
//  Created by Okasha Khan on 20/05/2021.
//

import Foundation
import RealmSwift


final class RealmManager {
    
    // MARK: - Setups
    
    static let shared = RealmManager()
    
    fileprivate var realm: Realm?

    init(){}
    
    enum RealmError: Error {
        
        case failedToGetData
        case failedToMakeConnection
    }
        
    // MARK: - Functions
    
    /// creating connection with realm database.
    /// - Returns: connection or nil
    public func createConnection() -> Realm? {
  
        do {
            
            let realm = try Realm()
            return realm
            
        } catch {
            
            print("Error Connection \(error)")
            return nil
        }
    }
    
    /// list number of repositories in the databse
    public func countRealmReposiotires(){
        
        guard let realm = self.createConnection() else{
            return
        }
        
        let repositories = realm.objects(RealmRepository.self)
        
        print("Total repositories \(repositories.count)")
    }
    
    /// Save reepository to realm database.
    /// - Note: it update if value is same due to unique key.
    /// - Parameter repository: repository
    public func saveRepository(repository:Repository){
        
        DispatchQueue.global(qos: .background).async {
            
            guard let realm = self.createConnection() else{
                return
            }
            
            do {
                
              try realm.write {
                    
                    let realmRepository =  RealmRepository(repository)
                       
                    realm.add(realmRepository, update: Realm.UpdatePolicy.modified)
                }
            }catch {
                
                print("Error Write \(error)")
            }
        }
    }
    
    /// The function return repositories from real repositories
    /// - Note: it is run on background thread and return on main thread
    /// - Returns: completion handler.
    public func getAllRealmRepositories(completion: @escaping (Result<[Repository], Error>) -> Void) {
        
       DispatchQueue.global(qos: .background).async {
            
            var repositories:[Repository] = []
            
            guard let realm = self.createConnection() else{
               
                completion(.failure(RealmError.failedToMakeConnection))
                return
            }
            
        let realmRepositories = realm.objects(RealmRepository.self).sorted(byKeyPath: "date", ascending: false)
            
            for repos in realmRepositories{
                
                let repository = Repository(id: repos.id ?? 0, name: repos.name ?? "", forks: repos.forks ?? 0, open_issues: repos.open_issues ?? 0, watchers_count: repos.watchers_count ?? 0, url: repos.url ?? "", owner: Owner(login: repos.owner?.login ?? "", avatar_url: repos.owner?.avatar_url ?? ""))
                
                repositories.append(repository)
                
            }
            
            DispatchQueue.main.async {
                completion(.success(repositories))
            }
        }
    
    }

    /// The function will delete all real saved repositories
    public func deleteAllRealRepositories(){
        
        guard let realm = self.createConnection() else{
            return
        }
        
        let repositories = realm.objects(RealmRepository.self)
        
        do {
            try realm.write {
                realm.delete(repositories)
                
                print("Deleted all realm repositories")
            }
        } catch {
            print("Error: deleting all files...", error)
        }
    }
    
    /// The function will mainitain limit of the realm reposiotries
    /// - Parameter value: limit no.
    public func preserverRealmReposotries(with value:Int){
        
        DispatchQueue.global(qos: .background).async {
            
            guard let realm = self.createConnection() else{
                return
            }
            
            let realmRepositories = realm.objects(RealmRepository.self).sorted(byKeyPath: "date", ascending: false)
            
            for (index, repo) in realmRepositories.enumerated(){
                
                if index > value {
                    
                    self.deleteThisRepo(with: repo)
                }
            }
        }
    }
    
    /// The function will delete specific repo object
    /// - Parameter repository: repo object
    public func deleteThisRepo(with repository: RealmRepository){
        
        guard let realm = self.createConnection() else{
            return
        }
        
        do {
            try realm.write {
                
                realm.delete(repository)
            }
        } catch {
            print("Error: Repo deletion", error)
        }
    }
}
