//
//  ApiManager.swift
//  GitSearch
//
//  Created by Okasha Khan on 19/05/2021.
//

import Foundation


final class APIManager {
    
    // MARK: - Setups
    
    static let shared = APIManager()
    
    private init() {}
    
    struct Constants {
        
        static let baseAPIURL = "https://api.github.com"
    }

    enum APIError: Error {
        
        case failedToGetData
        case failedToDownloadReadMeFile
    }

    enum HTTPMethod: String {
        
        case GET
        case PUT
        case POST
        case DELETE
    }
    
    

    
    // MARK: - API CALLERS
    
    
    /// The function will create url request and return repositories
    /// - Note: It format Json data into readable format
    /// - Note: It return success on main thread.
    /// - Parameters:
    ///   - query: search string
    ///   - completion: completion handler for response
    public func searchRepositories(with query: String, completion: @escaping (Result<[Repository], Error>) -> Void) {
        
        createRequest(
            with:  URL(string: Constants.baseAPIURL  + "/search/repositories?q=" + "\(query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")"),
            type: .GET
        ) { request in
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                
                guard let data = data, error == nil else {
                    
                    completion(.failure(APIError.failedToGetData))
                    return
                }
                
                do {
                    
                    let result = try JSONDecoder().decode(APIRepositoryResponse.self, from: data)
                   
                    DispatchQueue.main.async {
                        
                        completion(.success(result.items))
                    }
                }catch{
                    
                    completion(.failure(error))
                }
            }
            
            task.resume()
        }
    }
    
        
    /// The function will create url request and receive list of files
    /// - Note: It will call another function which basically download read md file.
    /// - Note: It will check if read md file exists or not
    /// - Note: It return success on main thread.
    /// - Parameters:
    ///   - url: file contents url
    ///   - completion: completion handler
    public func readMdExistFetchItwith (with url: String, completion: @escaping (Result<ReadMd, Error>) -> Void) {
    
        createRequest(
            with:  URL(string: url + "/contents/" ),
            type: .GET
        ) { request in
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                
                guard let data = data, error == nil else {
                    
                    completion(.failure(APIError.failedToGetData))
                    return
                }
                
                do {
                    
                    let result = try JSONDecoder().decode([APIReadMdResponse].self, from: data)
                    
                    var readMdFileAvailable = false
                    
                    for file in result {
                        
                        if file.name == "README.md" {
                            
                            readMdFileAvailable = true
                            self.downloadReadMdFile(with: file.download_url ?? "") { result in
                                switch result {

                                    case .success(let string):
                                        
                                        DispatchQueue.main.async {
                                            
                                            completion(.success(ReadMd(file: string)))
                                        }
                          
                                    case .failure(let error): completion(.failure(error))
                                }
                            }
                        }
                    }
                    
                    if !readMdFileAvailable { completion(.failure(APIError.failedToGetData)) }
                    
                }catch{
                    
                    completion(.failure(error))
                }
            }
            task.resume()
        }
    }

    
    /// The function will download readMd file from download url
    /// - Note: The function will return string format of read Md file
    /// - Parameters:
    ///   - url: actual url to download read md file
    ///   - completion: completion handler
    private func downloadReadMdFile(with url: String,completion:@escaping (Result<String,Error>) -> Void) {
        
        createRequest(
            with:  URL(string: url),
            type: .GET
        ) { request in

            let task = URLSession.shared.dataTask(with: request) { data, _, error in
               
                guard let data = data, error == nil else {
                    
                    completion(.failure(APIError.failedToDownloadReadMeFile))
                    return
                }
                
                let str = String(decoding: data, as: UTF8.self)
                completion(.success(str))
            }

            task.resume()
        }
    }
    
    
    
    
    
    // MARK: - Functions
    
    private func createRequest(
        with url: URL?,
        type: HTTPMethod,
        completion: @escaping (URLRequest) -> Void
    ) {
        
        guard let apiURL = url else { return }
        
        var request = URLRequest(url: apiURL)
        request.timeoutInterval = 30
        completion(request)
    }
    

}
